var keywordSelectedArray = [];

window.addEvent('domready', function() {
	var base_keyword_input = $('input[name="base_keyword"]');
	var base_keyword_input_popup = $('[name="base_keyword_popup"]');	
	
	$('#external-keyword-btn').on('click', function() {
		searchExtendedKeyword(base_keyword_input.val());
		$('[name="base_keyword_popup"]').val(
				$('[name="base_keyword"]').val());

		$(function() {
			$("#extended-search-dialog").dialog({
				modal : true,
				resizable: false,
				minWidth : 550,
				minHeight : 500,
				dialogClass : "",
				show : "fadeIn"
			});

			$('.ui-dialog-content').removeClass('hide');
			setListCount();
		});
	});
	
	$('#rd-dialog-button').on('click', function() {
		$(function() {
			$('#reuse-importer-dialog').dialog({
				modal: true,
				resizable: false,
				minWidth: 1200,
				minHeifht: 700,
				dialogClass: "",
				show: "fadeIn"
			});
			
			$('#reuse-importer-dialog').removeClass('hide');
			
		});
	});

	/* 체크된 Relatum submit */
	$('#external-keyword-submit-btn').on('click', function() {
		var list = '';

		$('[name="extended-keyword-selected"]:checked').each(
			function(i) {
				list += $(this).val() + ",";
			});

		$('[name="ext_rd_relation_list"]').val(list);
		$('#extended-search-dialog').dialog('close');
	});

	$('[name="base_keyword_popup"]').change(function() {
		$('[name="base_keyword"]').val($(this).val());
	});

	$('#collapseMore').on('hidden.bs.collapse', function() {
		$(this).find('select, input').attr('disabled', true);
		$(".multi-select").multiSelect('refresh');
	});

	$('#collapseMore').on('show.bs.collapse', function() {
		$(this).find('select, input').attr('disabled', false);
		$(".multi-select").multiSelect('refresh');
	});
	
	$('.datepicker').datepicker({
		format : 'yyyy-mm-dd',
		orientation: 'bottom auto'
	});	
	
	$(function() {
		"use strict";
		$(".multi-select").multiSelect();
	});
	
	$(document).ready(function() {
		$(".multi-select").multiSelect('deselect_all');
	});
	
	$("input[type='checkbox']").change(function() {
		var keyword = $(this).val();
		toggleAllCardBackground(keyword);
		
		var extended_keyword_result = document.getElementById('extended-keyword-result');
		if(this.checked) {
			if(this.name == "extended-keyword-selected") {
				addKeywordToExtendKeywordResult(keywordSelectedArray, keyword);
			}
		} else if(!this.checked) {
			if(this.name == "extended-keyword-selected") {
				//removeKeywordToExtendKeywordResult(keywordSelectedArray, removeItem);
				
				keywordSelectedArray = $.grep(keywordSelectedArray, function(value) {
					return value != keyword;
				});
				
				extended_keyword_result.innerHTML = "";
				$.each(keywordSelectedArray, function(index, value) {
					extended_keyword_result.innerHTML += "<span class=\"selected-keyword\" onclick=\"selectedKeywordClick('"+value+"');\" val=\""+value+"\">"+value+"</span>"
				});
			}
		}
	});
	
	$('.cardspan').click(function() {
		toggleCardBackground(this);
		var keyword = $(this).children(".cardspan-title").text();
		cardspanClick(keyword);
	});
	
	$('.grid-cardspan').click(function() {
		toggleCardBackground(this);
		var keyword = $(this).children(".cardspan-title").text();
		cardspanClick(keyword);
	});
	
	$('#import-all-btn').on('click',function(){		
		/*
		if($('#rd_list_table div').length ==0)
		{
			alert("No Import RD");
			return;
		}
		*/
		
		$('#reuse-importer-dialog').dialog('close');
		
		$('#comment_importer').dialog({
			modal : true,
			minWidth : 550,
			minHeight : 400,
			dialogClass : "",
			show : "fadeIn"
		});
		
		$('#comment_importer').removeClass('hide');	
	});
	
	$('#rd-select-btn').on('click', function() {
		var title_obj = $('#rd-title');
		var title = title_obj.text();
		var tr = document.createElement('tr');
		tr.innerHTML = "<td style='width: 5%;'><input type=checkbox id='selected-rd-checkbox' name=list></input></td><td style='width: 15%;'>917</td><td style='width: 69%;'>"+title+"</td>";
		var selected_rd_table = document.getElementById('rd-list-table');
		var selected_rd_tbody = selected_rd_table.tBodies.item(0);
		if($(selected_rd_tbody).children().length != 0) {
			var lastChild = selected_rd_tbody.lastChild;
			if($(lastChild).children('td').eq(1).text() == '917') {
				return;
			}
		}
		selected_rd_tbody.appendChild(tr);
	});
	
	$('#final-import-btn').on('click', function() {
		var comment_area = document.getElementById('comment');
		var comment_text = $(comment_area).val();
		var rd_comments_table = document.getElementById('rd-comments-table');
		var rd_comments_table_body = rd_comments_table.tBodies.item(0);
		
		if(comment_text == '') {
			$('#comment_importer').dialog('close');
			$('#reuse-importer-dialog').dialog('open');
			return;
		}
		
		var today = new Date();
		var date = today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var fullDate = date + " " + time;
		
		var comment_html = "<tr><td style='width: 18%;'>SW Nam</td><td style='width: 50%;'>"+comment_text+"</td><td style='width: 32%;text-align: center'>"+fullDate+"</td></tr>"
		
		$(rd_comments_table_body).append(comment_html);
		
		$(comment_area).val('');
		
		$('#comment_importer').dialog('close');
		$('#reuse-importer-dialog').dialog('open');
	});
	
	$('#remove-id-btn').on('click', function() {
		var idTable = $('#rd-list-table');
		var tbody = idTable.find('tbody');
		var tableLength = idTable.find('tr').length;
		var flag = 0;
		
		if(tableLength == 0) {
			alert("selected rd list is empty");
			return;
		}
		
		for(var i=0; i<tableLength;i++) {
			if(idTable.find('tr').eq(i).find('input').prop("checked") == true) {
				flag = 1;
			}
		}
		
		if(flag == 0) {
			alert("rd doesn't checked");
			return;
		}
		
		if(tableLength>0) {
			var checkedTRList = [];
			for(var j=0; j<tableLength;j++) {
				if(idTable.find('tr').eq(j).find('input').prop("checked") == true) {
					var tr = idTable.find('tr').eq(j);
					checkedTRList.push(tr);
				}
			}
			
			checkedTRList.each(function(value, index) {
				value.remove();
			});
		} 
		
	});
	
	$('#rd-input-event-clear-button').click(function() {
		var keyword = $('#search-input').val();
		$('#search-input').val("");
		var event_writer_div = document.getElementById('rd-input-event-writer');
		event_writer_div.innerHTML = "";
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text("Input keyword : " + keyword);
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
	
	$('#rd-list-event-clear-button').click(function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		event_writer_div.innerHTML = "";
	});
	
	$('#search-input').keydown(function(e) {
		var event_writer_div = document.getElementById('rd-input-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text(e.key + " / " + e.keyCode);
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
	
	var hoverStartTime = "";
	var hoverEndTime = "";
	
	$('.rd-list-title').on('mouseenter', function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		hoverStartTime = new Date();
		var timeText = hoverStartTime.getHours() + ":" +hoverStartTime.getMinutes() + ":" + hoverStartTime.getSeconds();
		$(span).text($(this).text() + " hover start at " + timeText + " / ");
		event_writer_div.appendChild(span);
	});
	
	$('.rd-list-title').on('mouseleave', function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		hoverEndTime = new Date();
		var timeText = hoverEndTime.getHours() + ":" +hoverEndTime.getMinutes() + ":" + hoverEndTime.getSeconds();
		$(span).text($(this).text() + " hover end at "+timeText + " / ");
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
		var span_between = document.createElement('span');
		var diff = new Date(hoverEndTime - hoverStartTime);
		var br_between = document.createElement('br');
		timeText = diff.getMinutes() + ":" + diff.getSeconds() + "." + diff.getMilliseconds();
		$(span_between).text("hover time : "+timeText);
		event_writer_div.appendChild(span_between);
		event_writer_div.appendChild(br_between);
	});
	
	$('.rd-list-title').click(function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text($(this).text() + " is clicked");
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
	
	var isFirstScroll = 0;
	var scrollStartTime = "";
	var scrollEndTime = "";
	
	$('.rd-list-container').on('scrollstart', function() {
		scrollStartTime = new Date();
		var event_writer_div = document.getElementById('rd-list-event-writer');
		if(isFirstScroll == 1) {
			var span_between = document.createElement('span');
			var br_between = document.createElement('br');
			var diff = new Date(scrollStartTime - scrollEndTime);
			timeText = diff.getMinutes() + ":" + diff.getSeconds() + "." + diff.getMilliseconds();
			$(span_between).text("scroll between time : " + timeText);
			event_writer_div.appendChild(span_between);
			event_writer_div.appendChild(br_between);
		}
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text("scroll start");
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
		
	});
	
	$('.rd-list-container').on('scrollstop', function() {
		isFirstScroll = 1;
		scrollEndTime = new Date();
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text("scroll stop");
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
	
	$('.rd-content').on('mousedown', function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		$(span).text($(this).text() +" is selected");
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
	
	$('.rd-list-page-number').click(function() {
		var event_writer_div = document.getElementById('rd-list-event-writer');
		var span = document.createElement('span');
		var br = document.createElement('br');
		var event_log = "page " + $(this).text() + " is clicked";
		if($(this).text() == "<-")  {
			event_log = "previous page is clicked";
		} else if($(this).text() == "->") {
			event_log = "next page is clicked";
		}
		$(span).text(event_log);
		event_writer_div.appendChild(span);
		event_writer_div.appendChild(br);
	});
});

function toggleAllCardBackground(keyword) {
	var card = $(".grid-cardspan>.cardspan-title[title='"+keyword+"']").parent();
	var gridCard = $(".cardspan>.cardspan-title[title='"+keyword+"']").parent();
	
	if($(card).hasClass('card-click-color')) {
		$(card).removeClass('card-click-color');
	} else {
		$(card).addClass('card-click-color');
	}
	
	if($(gridCard).hasClass('card-click-color')) {
		$(gridCard).removeClass('card-click-color');
	} else {
		$(gridCard).addClass('card-click-color');
	}
}

function toggleCardBackground(obj) {
	var keyword = $(obj).children(".cardspan-title").text();
	var otherObj = "";
	
	if($(obj).hasClass('cardspan')) {
		otherObj = $(".grid-cardspan>.cardspan-title[title='"+keyword+"']").parent();
	} else if ($(obj).hasClass('grid-cardspan')) {
		otherObj = $(".cardspan>.cardspan-title[title='"+keyword+"']").parent();
	}
		
	if($(obj).hasClass('card-click-color')) {
		$(obj).removeClass('card-click-color');
	} else {
		$(obj).addClass('card-click-color');
	}
	
	if($(otherObj).hasClass('card-click-color')) {
		$(otherObj).removeClass('card-click-color');
	} else {
		$(otherObj).addClass('card-click-color');
	}
	
}

function cardspanClick(keyword) {
	var extended_keyword_result = document.getElementById('extended-keyword-result');
		
	if($.inArray(keyword, keywordSelectedArray) != -1) {
		//removeKeywordToExtendKeywordResult(keywordSelectedArray, keyword);
		$(":checkbox[value=\""+keyword+"\"]").prop("checked", false);
		keywordSelectedArray = $.grep(keywordSelectedArray, function(value) {
			return value != keyword;
		});
		
		extended_keyword_result.innerHTML = "";
		$.each(keywordSelectedArray, function(index, value) {
			extended_keyword_result.innerHTML += "<span class=\"selected-keyword\" onclick=\"selectedKeywordClick('"+value+"');\" val=\""+value+"\">"+value+"</span>"
		});
	} else if($.inArray(keyword, keywordSelectedArray) == -1){
		$(":checkbox[value=\""+keyword+"\"]").prop("checked", "true");
		addKeywordToExtendKeywordResult(keywordSelectedArray, keyword);
	}
}

function setListCount() {
	var relatum_list = document.getElementById('extended-keyword-table');
	var list_body = $(relatum_list).children('tbody');
	var relatum_list_length = $(list_body).children("tr").size();
	var dialog_content_title = document.getElementById('relatum-dialog-content-title');
	dialog_content_title.classList.remove("pad100L");
	dialog_content_title.classList.add("pad80L");
	$(dialog_content_title).text("Relatum List("+relatum_list_length+")");
}

function setCardTableCount() {
	var relatum_list = document.getElementById('extended-keyword-table');
	var list_body = $(relatum_list).children('tbody');
	var relatum_list_length = $(list_body).children("tr").size();
	var dialog_content_title = document.getElementById('relatum-dialog-content-title');
	dialog_content_title.classList.remove("pad80L");
	dialog_content_title.classList.add("pad100L");
	$(dialog_content_title).text("Relatum card Table("+relatum_list_length+")");
}

function selectedKeywordClick(keyword) {
	var extended_keyword_result = document.getElementById('extended-keyword-result');
		
	if($.inArray(keyword, keywordSelectedArray) != -1) {
		//removeKeywordToExtendKeywordResult(keywordSelectedArray, keyword);
		$(":checkbox[value=\""+keyword+"\"]").prop("checked", false);
		keywordSelectedArray = $.grep(keywordSelectedArray, function(value) {
			return value != keyword;
		});
		
		extended_keyword_result.innerHTML = "";
		$.each(keywordSelectedArray, function(index, value) {
			extended_keyword_result.innerHTML += "<span class=\"selected-keyword\" onclick=\"selectedKeywordClick('"+value+"');\" val=\""+value+"\">"+value+"</span>"
		});
	} else if($.inArray(keyword, keywordSelectedArray) == -1){
		$(":checkbox[value=\""+keyword+"\"]").prop("checked", "true");
		addKeywordToExtendKeywordResult(keywordSelectedArray, keyword);
	}
}

function addKeywordToExtendKeywordResult(arr, keyword) {
	var extended_keyword_result = document.getElementById('extended-keyword-result');
	arr.push(keyword);
	extended_keyword_result.innerHTML = "";
	$.each(arr, function(index, value) {
		extended_keyword_result.innerHTML += "<span class=\"selected-keyword\" onclick=\"selectedKeywordClick('"+value+"');\" val=\""+value+"\">"+value+"</span>"
	});
}

/*
function removeKeywordToExtendKeywordResult(arr, keyword) {
	var extended_keyword_result = document.getElementById('extended-keyword-result');
	arr = $.grep(arr, function(value) {
		return value != keyword;
	});
	extended_keyword_result.innerHTML = "";
	$.each(arr, function(index, value) {
		extended_keyword_result.innerHTML += "<span val=\""+value+"\">"+value+", "+"</span>"
	});
}
*/

function qualityChangeBtnClick(clicked_id) {
	var ID = clicked_id;
	var rd_quality_container = document.getElementById('rd-quality-factors-container');
	var sd_quality_container = document.getElementById('sd-quality-factors-container');
	
	if(ID == 'rd-quality-change-btn') {
		rd_quality_container.classList.add('hide');
		sd_quality_container.classList.remove('hide');
	} else if(ID == 'sd-quality-change-btn') {
		rd_quality_container.classList.remove('hide');
		sd_quality_container.classList.add('hide');
	}
}

// Relatum Search Dialog : list <-> card
function changeBtnClick(clicked_id) {
	var ID = clicked_id;
	
	if(ID == 'listBtn') {
		setListCount();
		var table_header = document.getElementById('extended-keyword-table-header');
		table_header.style.display = 'block';
		var table_container = document.getElementById('extended-keyword-table-container');
		table_container.style.display = 'block';
		var card_container = document.getElementById('extended-keyword-card-container');
		card_container.style.display = 'none';
		var grid_card_container = document.getElementById('extended-keyword-card-container-2');
		grid_card_container.style.display = 'none';
	} else if(ID == 'cardBtn') {
		setCardTableCount();
		var table_header = document.getElementById('extended-keyword-table-header');
		table_header.style.display = 'none';		
		var table_container = document.getElementById('extended-keyword-table-container');
		table_container.style.display = 'none';
		var card_container = document.getElementById('extended-keyword-card-container');
		card_container.style.display = 'block';
		var grid_card_container = document.getElementById('extended-keyword-card-container-2');
		grid_card_container.style.display = 'none';
	} else if(ID == 'cardBtn-2') {
		setCardTableCount();
		var table_header = document.getElementById('extended-keyword-table-header');
		table_header.style.display = 'none';		
		var table_container = document.getElementById('extended-keyword-table-container');
		table_container.style.display = 'none';
		var card_container = document.getElementById('extended-keyword-card-container');
		card_container.style.display = 'none';
		var grid_card_container = document.getElementById('extended-keyword-card-container-2');
		grid_card_container.style.display = 'grid';
		setGridCol();
	}
}

function setGridCol() {
	var grid = document.getElementById('extended-keyword-card-container-2');
	var grid_children = $(grid).children('.grid-cardspan');
	//alert(grid_children.length);
	grid_children.each(function() {
		keyword = $(this).children('.cardspan-title').text();
		if(keyword.length <= 8) {
			this.classList.add('grid-3');
		} else if (keyword.length <= 16) {
			this.classList.add('grid-4');
		} else if (keyword.length <= 22) {
			this.classList.add('grid-5');
		} else if (keyword.length <= 30) {
			this.classList.add('grid-6');
		}
	});
}

/* extended keyword를 비동기 검색하여 table에 출력 */
function searchExtendedKeyword(keyword) {
	var extended_keyword_table = $('#extended-keyword-table');
	
	/*
	$.ajax({
		type : "POST",
		url : './rd/search/extended',
		data : {
			base_keyword : keyword
		},
		success : function(data) { /////////<-------------'list' variable: data//////////
			extended_keyword_table.find('tbody').empty();

			for (var i = 0; i < data.length; i++) {
				var tmpRelatum = data[i].word;
				var tmpSimilarity = data[i].similarity;
				if (parseInt(tmpSimilarity) == -200) {
					tmpSimilarity = "N/A";
				}
				extended_keyword_table
						.find('tbody')
						.append(
								'<tr><td style="text-algin:center;"><input type="checkbox" name="extended-keyword-selected" class="checkbox" value="'+tmpRelatum+'" /></td><td style="text-algin:center;">'
										+ data[i].word
										+ '</td><td style="text-algin:center;">'
										+ tmpSimilarity
										+ '</td><td style="text-algin:center;">&nbsp;&nbsp;</td></tr>');
			}
		},
		error : function(e) {
			console.log('ajax error');
		}
		
	});
	*/
}
