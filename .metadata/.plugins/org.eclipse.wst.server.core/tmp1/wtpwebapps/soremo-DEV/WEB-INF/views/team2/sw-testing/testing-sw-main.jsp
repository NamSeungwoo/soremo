<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
<style type="text/css">
.ui-dialog-titlebar-show:before {
	content: "\f106" !important
}

.ui-dialog-titlebar-hide:before {
	content: "\f107" !important
}
.ui-dialog, .ui-dialog-titlebar{
	background-color:#f5f5f5 !important;
}

</style>
</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>
			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="row">
										<div class="col-sm-9">
											<div class="content-box">
												<h3 class="content-box-header bg-default">Model View</h3>
												<div class="content-box-wrapper" id="contentList">
													<img class="img-responsive"
														src="${pageContext.servletContext.contextPath}/resources/assets/images/example-diagram.png" />
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="content-box">
												<h3 class="content-box-header bg-default">관련 RD</h3>
												<div class="content-box-wrapper" id="contentList">
													<table class="table">
														<thead>
															<tr>
																<th>#</th>
																<th>RD</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>1</td>
																<td>SDD-001</td>
															</tr>
															<tr>
																<td>2</td>
																<td>SDD-002</td>
															</tr>
															<tr>
																<td>3</td>
																<td>DetailedDesign001</td>
															</tr>
															<tr>
																<td>4</td>
																<td>DetailedDesign002</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="content-box">
												<h3 class="content-box-header bg-default">제약 사항</h3>
												<div class="content-box-wrapper">
													<table class="table">
														<thead>
															<tr>
																<th>사전 조건</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																	<ul>
																		<li>테스트의 범위</li>
																		<li>연관성 규칙들이</li>
																		<li>프로젝트의 외적 정보 및 품질</li>
																	</ul>
																</td>
															</tr>
														</tbody>
													</table>
													<table class="table">
														<thead>
															<tr>
																<th>후행 조건</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																	<ul class="">
																		<li>테스트 리포트 산출</li>
																	</ul>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="content-box">
												<h3 class="content-box-header bg-default">품질 속성</h3>
												<div class="content-box-wrapper"></div>
											</div>
											<div class="button-pane mrg20T">
												<div class="form-group">
													<select name="coverage" class="chosen-select">
														<optgroup label="Select Coverage">
															<option>Basis path coverage</option>
															<option>Simple path coverage</option>
															<option>Activity path coverage</option>
														</optgroup>
													</select>

												</div>
												<button id="testcase-btn" class="btn btn-gray btn-block">테스트
													케이스</button>
												<button id="model-validation-btn"
													class="btn btn-gray btn-block">모델 검증</button>
												<button id="save-btn" class="btn btn-info btn-block">저장</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="hide" id="rd-explorer-dialog" title="RD 탐색기">
			<div id="rd-tree">
				<ul>
					<li id="100">Planning
						<ul>
							<li id="101">1</li>
							<li id="102">2</li>
						</ul>
					</li>
					<li id="200">Test Design and Implementation
						<ul>
							<li id="201">Identify Feature List</li>
							<li id="202">Derive Test Conditions</li>
							<li id="203">Derive Test Conditions</li>
							<li id="204">Derive Test Conditions</li>
							<li id="205">Derive Test Conditions</li>
						</ul>
					</li>
					<li id="300">Test Enviroment Set-Up and Maintenance</li>
					<li id="400">Test Execution
						<ul>
							<li>Execute Test Procedures</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div class="hide" id="sw-testcase-dialog" title="테스트 케이스">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>테스트 단계</th>
						<th>품질 속성</th>
						<th>RD ID</th>
						<th>입력값</th>
						<th>예상 결과값</th>
						<th>비고</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>시스템 테스트</td>
						<td>기능성</td>
						<td>F-010</td>
						<td>입력값</td>
						<td>예상 결과값</td>
						<td>M001 관련</td>
					</tr>
					<tr>
						<td>2</td>
						<td>시스템 테스트</td>
						<td>기능성</td>
						<td>F-010</td>
						<td>입력값</td>
						<td>예상 결과값</td>
						<td>M001 관련</td>
					</tr>
				</tbody>
			</table>
		</div>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/chosen/chosen.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/chosen/chosen-demo.js"></script>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/resizable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/draggable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/selectable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog.js"></script>
		<script type="text/javascript">
			$(document).ready(
					function() {
						$('#rd-tree').jstree();

						var rd_explorer_dialog = $("#rd-explorer-dialog");
						var idx = $(this).attr('data-id');

						$(function() {
							rd_explorer_dialog.dialog({
								modal : false,
								minWidth : 160,
								minHeight : 340,
								dialogClass : "",
								show : "fadeIn",
								closeOnEscape : false,
								position : {
									my : 'left bottom',
									at : 'left center',
									of : document
								},
								close : function(event, ui) {
									rd_explorer_dialog.toggleClass('hide');
									rd_explorer_dialog.siblings('.ui-dialog-titlebar').find('.ui-dialog-titlebar-close').toggleClass(
											'ui-dialog-titlebar-hide');
									$("#rd-explorer-dialog").dialog("open");
								}
							});
							rd_explorer_dialog.removeClass('hide');
						});
					});
		</script>

		<script type="text/javascript">
			var sw_testcase_dialog = $('#sw-testcase-dialog');

			$('#testcase-btn').on('click', function() {
				$(function() {
					sw_testcase_dialog.dialog({
						modal : false,
						minWidth : 700,
						minHeight : 340,
						show : "fadeIn",
						resizable: false,
						closeOnEscape : true
					});
					sw_testcase_dialog.removeClass('hide');
				});
			});
		</script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>



</body>

</html>
