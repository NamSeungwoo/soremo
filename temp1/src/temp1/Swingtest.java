package temp1;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

public class Swingtest {
	public static void main(String[] args) {
		JFrame frame = new JFrame("team 1 - Libre");
		frame.setSize(500, 400);
		JButton b = new JButton("start");
		Container cont = new Container();
		cont.setSize(500,500);
		frame.add(cont);
		cont.add(b);
		b.setBounds(50,100,95,30);
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setting();
			}
		});
		frame.setVisible(true);
//	    new jxBrowser();
	}
	public static void setting(){
		Browser browser = new Browser();
	    BrowserView view = new BrowserView(browser);
	    JFrame frame = new JFrame("team 3 - App application");   

	    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	    frame.add(view, BorderLayout.CENTER);
	    frame.setSize(1600, 1500);
	    frame.setLocationRelativeTo(null);
	    frame.setVisible(true);

	    browser.loadURL("http://localhost:8080/soremo");
	}
	
	
}