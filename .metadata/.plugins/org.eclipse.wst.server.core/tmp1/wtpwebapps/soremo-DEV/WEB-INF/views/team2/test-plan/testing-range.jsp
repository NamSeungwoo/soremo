<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>

			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<div id="100" class="content-box testing-range-content">
											<h3 class="content-box-header bg-blue">
												<i class="glyph-icon icon-users"></i> S/W 연구 개발 일반
												<div class="header-buttons-separator">
													<a href="#" class="icon-separator" data-id="100"> <i
														class="glyph-icon icon-check"></i>
													</a>
												</div>
											</h3>
											<div class="content-box-wrapper">
												<table class="table">
													<tr>
														<th width="30%">분야</th>
														<td>연구 일반</td>
													</tr>
													<tr>
														<th width="30%">관련 표준</th>
														<td>없음</td>
													</tr>
													<tr>
														<th width="30%">관련 가이드라인</th>
														<td>한국 연구재단</td>
													</tr>
													<tr>
														<th width="30%">관련 문서현황</th>
														<td>3,000</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div id="200" class="content-box testing-range-content">
											<h3 class="content-box-header bg-green">
												<i class="glyph-icon icon-medkit"></i> 의료기기 S/W
												<div class="header-buttons-separator">
													<a href="#" class="icon-separator" data-id="200"> <i
														class="glyph-icon icon-check"></i>
													</a>
												</div>
											</h3>
											<div class="content-box-wrapper">
												<table class="table">
													<tr>
														<th width="30%">분야</th>
														<td>연구 일반</td>
													</tr>
													<tr>
														<th width="30%">관련 표준</th>
														<td>없음</td>
													</tr>
													<tr>
														<th width="30%">관련 가이드라인</th>
														<td>한국 연구재단</td>
													</tr>
													<tr>
														<th width="30%">관련 문서현황</th>
														<td>3,000</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div id="300" class="content-box testing-range-content">
											<h3 class="content-box-header bg-yellow">
												<i class="glyph-icon icon-car"></i> 자동차 S/W
												<div class="header-buttons-separator">
													<a href="#" class="icon-separator" data-id="300"> <i
														class="glyph-icon icon-check"></i>
													</a>
												</div>
											</h3>
											<div class="content-box-wrapper">
												<table class="table">
													<tr>
														<th width="30%">분야</th>
														<td>연구 일반</td>
													</tr>
													<tr>
														<th width="30%">관련 표준</th>
														<td>없음</td>
													</tr>
													<tr>
														<th width="30%">관련 가이드라인</th>
														<td>한국 연구재단</td>
													</tr>
													<tr>
														<th width="30%">관련 문서현황</th>
														<td>3,000</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div id="400" class="content-box testing-range-content">
											<h3 class="content-box-header bg-green">
												<i class="glyph-icon icon-medkit"></i> 의료기기 S/W
												<div class="header-buttons-separator">
													<a href="#" class="icon-separator" data-id="400"> <i
														class="glyph-icon icon-check"></i>
													</a>
												</div>
											</h3>
											<div class="content-box-wrapper">
												<table class="table">
													<tr>
														<th width="30%">분야</th>
														<td>연구 일반</td>
													</tr>
													<tr>
														<th width="30%">관련 표준</th>
														<td>없음</td>
													</tr>
													<tr>
														<th width="30%">관련 가이드라인</th>
														<td>한국 연구재단</td>
													</tr>
													<tr>
														<th width="30%">관련 문서현황</th>
														<td>3,000</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="button-pane mrg20T">
									<button class="btn btn-info">저장</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$('.testing-range-content').find('.icon-separator').on('click', function() {
				$('.testing-range-content').addClass('opacity-40');
				
				var id = $(this).attr('data-id');
				console.log(id);
				$('#'+id).removeClass('opacity-40');
			});
		</script>
		



		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
