<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
<style type="text/css">
.rd-name {
	cursor: pointer
}

.popover-button-default {
	cursor: pointer
}
</style>

</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>
			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="content-box">
										<h3 class="content-box-header bg-default">문서 산출물 테스트 결과</h3>
										<div class="content-box-wrapper">
											<div class="row demo-margin">
												<div class="col-md-3">
													<div class="chart-alt-1 easyPieChart" data-percent="90"
														style="width: 100px; height: 100px; line-height: 80px;">
														적합
														<canvas width="100" height="100"></canvas>
													</div>
													<div
														class="text-center font-bold mrg10A label no-border center-div display-block font-black">연구개발
														과제 계획서</div>
												</div>
												<div class="col-md-3">
													<div class="chart-alt-1 easyPieChart" data-percent="95"
														style="width: 80px; height: 80px; line-height: 80px;">
														완료
														<canvas width="100" height="100"></canvas>
													</div>
													<div
														class="text-center font-bold mrg10A label no-border center-div display-block font-black">연차보고서(1차년도)</div>
												</div>
												<div class="col-md-3">
													<div class="chart-alt-1 easyPieChart" data-percent="45"
														style="width: 80px; height: 80px; line-height: 80px;">
														미흡
														<canvas width="100" height="100"></canvas>
													</div>
													<div
														class="text-center font-bold mrg10A label no-border center-div display-block font-black">차량
														고장모드 및 영향분석 보고서</div>
												</div>
												<div class="col-md-3">
													<div class="chart-alt-1 easyPieChart" data-percent="70"
														data-bar-color="#2980b9"
														style="width: 80px; height: 80px; line-height: 80px;">
														양호
														<canvas width="100" height="100"></canvas>
													</div>
													<div
														class="text-center font-bold mrg10A label no-border center-div display-block font-black">소프트웨어
														요구사항 명세서(SRS)</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="example-box-wrapper">
									<div class="content-box">
										<h3 class="content-box-header bg-default">문서 산출물 테스트 결과</h3>
										<div class="content-box-wrapper">
											<table id="rd-test-result-table" class="table table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>문서 이름</th>
														<th>결과</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td class="rd-name" data-id="100">연구개발 과제 계획서</td>
														<td>우수</td>
													</tr>
													<tr>
														<td>2</td>
														<td class="rd-name" data-id="200">연차보고서(1차년도)</td>
														<td>미흡</td>
													</tr>
													<tr>
														<td>3</td>
														<td class="rd-name" data-id="300">차량 고장모드 및 영향분석 보고서</td>
														<td>미흡(5 문제점)</td>
													</tr>
													<tr>
														<td>4</td>
														<td class="rd-name" data-id="400">차량 고장모드 및 영향분석 보고서</td>
														<td>미흡(5 문제점)</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="hide" id="result-dialog" title="연구 개발 과제 계획서">
			<div class="pad10A">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>문서 이름</th>
							<th>결과</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>연구개발 과제 계획서</td>
							<td>우수</td>
						</tr>
						<tr>
							<td>2</td>
							<td>연차보고서(1차년도)</td>
							<td class="popover-button-default"  data-trigger="hover"
								data-content="
									<div class='row'>
										<div class='col-xs-9 text-left'>연구 책임자의 명세</div><div class='col-xs-3 text-right'><i class='glyph-icon icon-check-circle font-green font-size-16'></i></div>
										<div class='col-xs-9 text-left'>연구 책임자의 명세</div><div class='col-xs-3 text-right'><i class='glyph-icon icon-minus-circle font-red font-size-16'></i></div>
										<div class='col-xs-12'><small>참고:IEC 12707 및 원천연구과제템플릿에서는...</small></div>
									</div>"
								title="연구 수행 조직과 업무 분담 책임에 대한" data-placement="top">미흡 <i
								class="glyph-icon icon-plus-circle font-red font-size-14"></i></td>
						</tr>
						<tr>
							<td>3</td>
							<td>차량 고장모드 및 영향분석 보고서</td>
							<td>미흡(5 문제점)</td>
						</tr>
						<tr>
							<td>4</td>
							<td>차량 고장모드 및 영향분석 보고서</td>
							<td>미흡(5 문제점)</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/resizable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/draggable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/selectable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog.js"></script>
		<script type="text/javascript">
			$('.rd-name').on('click', function() {
				var idx = $(this).attr('data-id');
				console.log('선택된 테스트 결과의 id:' + idx);
				$(function() {
					$("#result-dialog").dialog({
						modal : !0,
						minWidth : 500,
						minHeight : 200,
						dialogClass : "",
						show : "fadeIn",

					});
					$(".ui-widget-overlay").addClass("bg-black opacity-60");
					$('.ui-dialog-content').removeClass('hide');
				});

			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
