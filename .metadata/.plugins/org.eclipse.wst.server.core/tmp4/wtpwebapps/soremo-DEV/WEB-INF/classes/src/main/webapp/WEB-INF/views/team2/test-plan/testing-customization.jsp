<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>

			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<h3 class="title-hero">테스트 프로세스 수준</h3>
								<div class="example-box-wrapper">
									<div id="form-wizard-1">
										<ul>
											<li class="active"><a href="#tab1" data-toggle="tab">Lv.2</a></li>
											<li><a href="#tab2" data-toggle="tab">Lv.3</a></li>
											<li><a href="#tab3" data-toggle="tab">Lv.4</a></li>
											<li><a href="#tab4" data-toggle="tab">Lv.5</a></li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="tab1">
												<div class="row">
													<div class="col-md-6" style="min-height:600px">
														<div class="content-box">
															<h3 class="content-box-header bg-default">작업 목록</h3>
															<div class="content-box-wrapper">
																<div id="testing-list">
																	<ul>
																		<li id="100">Planning
																			<ul>
																				<li id="101">1</li>
																				<li id="102">2</li>
																			</ul>
																		</li>
																		<li id="200">Test Design and Implementation
																			<ul>
																				<li id="201">Identify Feature List</li>
																				<li id="202">Derive Test Conditions</li>
																				<li id="203">Derive Test Conditions</li>
																				<li id="204">Derive Test Conditions</li>
																				<li id="205">Derive Test Conditions</li>
																			</ul>
																		</li>
																		<li id="300">Test Enviroment Set-Up and
																			Maintenance</li>
																		<li id="400">Test Execution
																			<ul>
																				<li>Execute Test Procedures</li>
																			</ul>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6" style="">
														<div class="content-box">
															<h3 class="content-box-header bg-default">설명</h3>
															<div class="content-box-wrapper" id="contentList">

															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab2"></div>
											<div class="tab-pane" id="tab3">
												<div class="content-box">
													<h3 class="content-box-header bg-green">Third</h3>
													<div class="content-box-wrapper"></div>
												</div>
											</div>
											<div class="tab-pane" id="tab4">
												<div class="content-box">
													<h3 class="content-box-header bg-blue-alt">Forth</h3>
													<div class="content-box-wrapper">Lorem ipsum dolor
														sic amet dixit tu.</div>
												</div>
											</div>
											<div class="button-pane mrg20T">
												<button class="btn btn-info">저장</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/wizard/wizard.js"></script>
		<script type="text/javascript">
			$("#form-wizard-1").bootstrapWizard({
				tabClass : "nav nav-pills"
			});
		</script>

		<script type="text/javascript">
			$('#testing-list').jstree();
		</script>

		<script type="text/javascript">
			$('#testing-list li')
					.on(
							'click',
							function() {
								$
										.ajax(
												{
													url : "${pageContext.servletContext.contextPath}/testing/get/task/details/"
															+ $(this)
																	.attr("id"),
													dataType : "json"
												})
										.success(
												function(data) {
													var elem = $('#contentList');

													var htmlData = '';

													for (var i = 0; i < data.length; i++) {
														htmlData += '<div class="content-box no-border">';
														htmlData += '<h4 class="clearfix">['
																+ data[i]['activityName']
																+ ']</h4>';
														htmlData += '<ul class="content-box-wrapper">';

														for (var j = 0; j < data[i]['taskList'].length; j++) {
															htmlData += '<li><strong>'
																	+ data[i]['taskList'][j].taskName
																	+ ': </strong>'
																	+ data[i]['taskList'][j].content
																	+ '</li>';
														}
														htmlData += '</ul><div class="divider"></div></div>';
													}

													elem.html(htmlData);

												}).done(function() {
											$(this).addClass("done");
										});
							});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
