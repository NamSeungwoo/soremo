<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
<style type="text/css">
.ui-sortable-handle .content-box-header {
	cursor: move
}
</style>

</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>

			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="column-sort ui-sortable">
									<div class="content-box ui-sortable-handle">
										<h3 class="content-box-header bg-default">연구 개발 과제 계획서</h3>
										<div class="content-box-wrapper" id="contentList">
											<table class="table table-responsive">
												<thead>
													<tr>
														<th>번호</th>
														<th>Design Guideline</th>
														<th>Evaluation</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>연구 목적과 필요성, 연구 동향과 사업 내용의 부합</td>
														<td>예</td>
														<td><input type="checkbox" class="input-switch-alt"
															checked>
													</tr>
													<tr>
														<td>2</td>
														<td>연구 수행 조직과 업무 분담, 책임에 대해 적절한 기술</td>
														<td class="font-red">아니오</td>
														<td><input type="checkbox" class="input-switch-alt">
													</tr>
													<tr>
														<td>3</td>
														<td>연구 추진 전략의 적절한 기술</td>
														<td class="font-red">아니오</td>
														<td><input type="checkbox" class="input-switch-alt">
													</tr>
													<tr>
														<td>4</td>
														<td>연구 목적과 필요성, 연구 동향과 사업 내용의 부합</td>
														<td colspan="2">Unneccessary</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="column-sort ui-sortable">
									<div class="content-box ui-sortable-handle">
										<h3 class="content-box-header bg-default">연구 개발 과제 계획서</h3>
										<div class="content-box-wrapper" id=""></div>
									</div>
								</div>
								<div class="column-sort ui-sortable">
									<div class="content-box ui-sortable-handle">
										<h3 class="content-box-header bg-default">차량 고장모드 및 영향 분석 보고서</h3>
										<div class="content-box-wrapper" id=""></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<script type="text/javascript"
				src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
			<script type="text/javascript">
				/* Sortable elements */

				$(function() {
					"use strict";
					$(".sortable-elements").sortable();
				});

				$(function() {
					"use strict";
					$(".column-sort").sortable({
						connectWith : ".column-sort"
					});
				});
			</script>
			<script type="text/javascript">
				$('#testing-list').jstree();
			</script>



			<script type="text/javascript"
				src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
		</div>
</body>

</html>
