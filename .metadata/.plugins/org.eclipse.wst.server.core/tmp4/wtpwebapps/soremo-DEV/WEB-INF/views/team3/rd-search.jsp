<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="../commons/meta.jsp"%>
<%@ include file="../commons/core-js.jsp"%>

<!-- core css 정의 -->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/yonsei/css/multi-select.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/yonsei/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/yonsei/css/bootstrap-datepicker3.standalone.css">
<%@ include file="../commons/core-css.jsp"%>

<!-- user css 정의 -->
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/yonsei/css/soremo_yonsei.css">

<!-- resource 정의 -->
<link rel="shortcut icon" href="${pageContext.servletContext.contextPath}/resources/yonsei/resources/assets/images/icon/favicon.png">	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheert" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/less/glyphicons.less">

<!-- core javascript 정의 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/yonsei/js/MooTools-Core-1.6.0.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/yonsei/js/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/yonsei/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/yonsei/js/bootstrap-datepicker.js"></script>
	
<!-- user javascript 정의 -->
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/yonsei/js/soremo_yonsei.js"></script>

<style type="text/css">

#external-keyword-table td, #external-keyword-table th {
	text-align: center
}

</style>
</head>

<body>
	<div id="sb-site">
		<%@ include file="../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../commons/ui/loader.jsp"%>
		<div id="page-wrapper">
			<%@ include file="../commons/ui/header-ui.jsp" %>
			<%@ include file="../commons/ui/sidemenu-ui.jsp" %>
			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="content-box">
							<!-- 
	                            TODO: Base Keyword 와 Extended Keyword 통해 호출할 API (action 속성 ) 
	                        -->
							<form:form id="searchForm"
								class="form-horizontal pad15L pad15R bordered-row"
								modelAttribute="searchDataModel" method="post"
								action="">
								<div class="form-group remove-border">
								<div class="center">
									<label class="col-sm-2 control-label">Base Keyword:</label>
									<div class="col-sm-10">
										<div class="input-group">
											<form:input type="text" id="base-keyword" class="form-control"
														path="base_keyword" placeholder="Base Keyword"></form:input>
											<span class="input-group-btn">
												<button id="external-keyword-btn" class="y-btn"
													type="button">Relatum Search</button>
											</span>
										</div>
									</div>
									</div>
								</div>
								<div class="form-group remove-border">
									<label class="col-sm-2 control-label">Extended Keyword:</label>
									<div id="extended-keyword-wrap" class="col-sm-10">
										<input name="ext_rd_relation_list" class="form-control" />
									</div>
								</div>
								<div class="panel-group text-center" id="accordion">
									<a id="searchExtended" data-toggle="collapse"
										data-target="#collapseMore" href="#" aria-expanded="true">
										More Options <i class="glyph-icon glyphicon-plus"></i>
									</a>
								</div>
								<div id="collapseMore" class="panel-collapse collapse"
									aria-expanded="true">

									<div class="form-group">
										<label class="col-sm-2 control-label">Target RD Type:</label>
										<div class="col-sm-10">
											<div class="input-group">
												<select multiple="multiple" class="multi-select"
													name="ext_rd_type" disabled>
													<c:forEach items="${rdTypeList}" var="item">
														<option>${item}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">검색범위(시작일):</label>
										<div class="col-sm-4">
											<div class="input-group">
												<div class="input-prepend input-group">
													<span class="add-on input-group-addon"> 
														<span class="glyphicon glyphicon-calendar"></span>
													</span> 
													<input  name="ext_rd_start_date"
														value="${searchDataModel.ext_rd_start_date}"
														class="datepicker form-control"
														data-date-format="yyyy-mm-dd"  />
												</div>
											</div>
										</div>
										<label class="col-sm-2 control-label">검색범위(종료일):</label>
										<div class="col-sm-4">
											<div class="input-group">
												<div class="input-prepend input-group">
													<span class="add-on input-group-addon"> 
														<span class="glyphicon glyphicon-calendar"></span>
													</span> 
													<input name="ext_rd_end_date"
														value="${searchDataModel.ext_rd_end_date}"
														class="datepicker form-control"
														data-date-format="yyyy-mm-dd" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="button-pane mrg20T">
									<input type="button" class="btn btn-info" id="search-result-btn" value="검색">
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="hide" id="extended-search-dialog" title="Relatum Select">
			<div class="form-group pad5T pad10B">
				<label class="col-sm-3 control-label mar10T pad5L">Base Keyword:</label>
				<div class="col-sm-9 pad0R pad0L">
					<div class="input-group">
						<input type="text" class="form-control" name="base_keyword_popup"
							placeholder="Base Keyword"> <span class="input-group-btn">
							<button id="external-keyword-pop-btn" class="y-btn" style="font-size: 14px"
								type="button">Search</button>
						</span>
					</div>
				</div>
			</div>

			<div class="container search-dialog-container">
				<div class="form-group pad5T pad10B center">
					<label id="relatum-dialog-content-title" class="col-sm-9 control-label pad60L">Relatum List</label>
					<div class="col-sm-3 right">
						<span id="listBtn" class="glyphicon glyphicon-list pad10R changeBtn" onClick="changeBtnClick(this.id)"></span>
						<span id="cardBtn" class="glyphicon glyphicon-th-large pad10R changeBtn" onClick="changeBtnClick(this.id)"></span>
					</div>
				</div>
				<div class="divider"></div>
				<table id="extended-keyword-table-header" class="table table-hover mar0B">
					<thead>
						<tr>
							<th style="width: 10%;"></th>
							<th style="width: 68%;">Relatum</th>
							<th style="width: 19%;">Similarity</th>
							<th style="width: 3%;"></th>
						</tr>
					</thead>
				</table>
				<div id="extended-keyword-table-container" class="relatum-keyword-container">
					<table id="external-keyword-table" class="table table-hover">
						<tbody>
						</tbody>
					</table>
				</div>
				<div id="extended-keyword-card-container" class="relatum-card-container container" style="display:none;">
				</div>
				<div class="divider"></div>
				<div id="extended-keyword-result" style="height: 100px;">
				</div>
				
			</div>
						
			<div class="button-panel mrg20T pad0B">
				<button class="btn btn-info" id="external-keyword-submit-btn"
					type="submit">확인</button>
			</div>
		</div>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/resizable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/draggable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/selectable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog.js"></script>
		<script type="text/javascript">
			var base_keyword_input = $('input[name="base_keyword"]');
			var base_keyword_input_popup = $('[name="base_keyword_popup"]');

            // cbnu - 2017/12  --->
            // communication to the method to choose the RD type preference with mouse click
            $('#search-result-btn').on('click',
                function() {
                    $.ajax(
                        {
                            type : "POST",
                            url : '${pageContext.servletContext.contextPath}/rd/search/visualization',
                            data : {
                                base_keyword : base_keyword_input.val()
                            },
                            success : function(data) {
                                redirectToResult(data, 1);
                            },
                            error : function(e) {
                                console.log('ajax error');
                            }
                        });
                });

            // communication to the method to choose the RD type preference with enter-key press
            $('#base-keyword').keypress(
                function(e){
                    if(e.which == 13 || e.KeyCode == 13){
                        $.ajax(
                            {
                                type : "POST",
                                url : '${pageContext.servletContext.contextPath}/rd/search/visualization',
                                data : {
                                    base_keyword : base_keyword_input.val()
                                },
                                success : function(data) {
                                    redirectToResult(data, 1);
                                },
                                error : function(e) {
                                    console.log('ajax error');
                                }
                            });
                    }
                });

            // move to search-result page
            function redirectToResult(base, page) {
                $('#searchForm').attr(
                    'action',
                    '${pageContext.servletContext.contextPath}/rd/search/result/'
                    + base + '/' + page).submit();
            }
            // <--- cbnu - 2017/12


			$('#external-keyword-btn').on(
					'click',
					function() {
						searchExtendedKeyword(base_keyword_input.val());
						$('[name="base_keyword_popup"]').val(
								$('[name="base_keyword"]').val());

						$(function() {
							$("#extended-search-dialog").dialog({
								modal : true,
								minWidth : 500,
								minHeight : 340,
								dialogClass : "",
								show : "fadeIn"
							});

							$('.ui-dialog-content').removeClass('hide');
						});
					});

			/* Dialog의 search button */
			$('#external-keyword-pop-btn').on('click', function() {
				searchExtendedKeyword(base_keyword_input_popup.val());
			});

			/* Dialog의 search input */
			$('[name="base_keyword_popup"]').on('keydown', function(e) {
				if (e.keyCode == 13)
					searchExtendedKeyword(base_keyword_input_popup.val());
			});

			/* 체크된 Relatum submit */
			$('#external-keyword-submit-btn').on(
					'click',
					function() {
						var list = '';

						$('[name="extended-keyword-selected"]:checked').each(
								function(i) {
									list += $(this).val() + ",";
								});

						$('[name="ext_rd_relation_list"]').val(list);
						$('#extended-search-dialog').dialog('close');
					});

			$('[name="base_keyword_popup"]').change(function() {
				$('[name="base_keyword"]').val($(this).val());
			});

			/* extended keyword를 비동기 검색하여 table에 출력 */
			function searchExtendedKeyword(keyword) {
				var extended_keyword_table = $('#external-keyword-table');

				$
						.ajax({
							type : "POST",
							url : '${pageContext.servletContext.contextPath}/rd/search/extended',
							data : {
								base_keyword : keyword
							},
							success : function(data) { /////////<-------------'list' variable: data//////////
								extended_keyword_table.find('tbody').empty();

								for (var i = 0; i < data.length; i++) {
									var tmpRelatum = data[i].word;
									var tmpSimilarity = data[i].similarity;
									if (parseInt(tmpSimilarity) == -200) {
										tmpSimilarity = "N/A";
									}
									extended_keyword_table
											.find('tbody')
											.append(
													'<tr><td style="width: 10%;"><input type="checkbox" name="extended-keyword-selected" class="checkbox" value="'+tmpRelatum+'" /></td><td style="width: 71%;">'
															+ data[i].word
															+ '</td><td style="width: 19%;">'
															+ tmpSimilarity
															+ '</td></tr>');
								}
							},
							error : function(e) {
								console.log('ajax error');
							}
						});
			}
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/multi-select/multiselect.js"></script>
		<script type="text/javascript">
			$(function() {
				"use strict";
				$(".multi-select").multiSelect();
				$(".ms-container").append(
						'<i class="glyph-icon icon-exchange"></i>');
			});
			$(document).ready(function() {
				$(".multi-select").multiSelect('deselect_all');
			});
		</script>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datepicker/datepicker.js"></script>
		<script type="text/javascript">
			$(function() {
				"use strict";
				$('.bootstrap-datepicker').bsdatepicker({
					format : 'yyyy-mm-dd'
				});
			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tabs-ui/tabs.js"></script>
		<script type="text/javascript">
			$(function() {
				"use strict";
				$(".nav-tabs").tabs();
			});

			$(function() {
				"use strict";
				$(".tabs-hover").tabs({
					event : "mouseover"
				});
			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>

		<script type="text/javascript">
			$('#collapseMore').on('hidden.bs.collapse', function() {
				$(this).find('select, input').attr('disabled', true);
				$(".multi-select").multiSelect('refresh');
			});

			$('#collapseMore').on('show.bs.collapse', function() {
				$(this).find('select, input').attr('disabled', false);
				$(".multi-select").multiSelect('refresh');
			});
		</script>
	</div>
</body>

</html>
