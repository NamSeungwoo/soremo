<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- core javascript 정의 -->

<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets-minified/js-core.js"></script>
	
	
<script type="text/javascript">
	$(window).load(function() {
		setTimeout(function() {
			$('#loading').fadeOut(400, "linear");
		}, 300);
	});
</script>