<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- core css 정의 -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${pageContext.servletContext.contextPath}/resources/assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${pageContext.servletContext.contextPath}/resources/assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${pageContext.servletContext.contextPath}/resources/assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="${pageContext.servletContext.contextPath}/resources/assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath}/resources/assets/images/icons/favicon.png">
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/themes/default/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets-minified/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets-minified/style-yonsei.css">
