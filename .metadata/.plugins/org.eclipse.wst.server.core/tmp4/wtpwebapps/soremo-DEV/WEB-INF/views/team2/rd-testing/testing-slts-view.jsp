<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/select.dataTables.min.css">
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<style type="text/css">
.progress-overlay {
	background:
		url(${pageContext.servletContext.contextPath}/resources/assets/images/animated-overlay.gif);
	!
	important
}
#datatable-slts td {
	text-align: center
}
</style>

</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>
			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="column-sort ui-sortable">
										<table cellpadding="0" cellspacing="0" border="0"
											class="table table-striped table-bordered"
											id="datatable-slts">
											<thead>
												<tr>
													<th></th>
													<th>대상 RD 종류</th>
													<th>평가 지침</th>
													<th>매트릭</th>
													<th>매트릭 종류</th>
													<th>RLIM</th>
													<th>비고</th>
												</tr>
											</thead>
											<tbody>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
												<tr class=" ">
													<td></td>
													<td>연구개발과제계획서</td>
													<td>DGM-001-001</td>
													<td>Metric-001-001</td>
													<td class="center">Semantic</td>
													<td class="center">UD-012-001</td>
													<td>-</td>
												</tr>
												<tr class="even ">
													<td></td>
													<td>SRS</td>
													<td>DGM-013-051</td>
													<td>Metric-013-011</td>
													<td class="center">Binary</td>
													<td class="center">-</td>
													<td>-</td>
												</tr>
											</tbody>
										</table>


										<div class="button-pane mrg20T row">
											<div class="col-xs-6 text-left">
												<button class="btn btn-info">SLTS 추출</button>
											</div>
											<div class="col-xs-6 text-right">
												<button id="doRDTest" class="btn btn-info">문서 산출물
													테스트 실행</button>
											</div>
										</div>


									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div id="loader-dialog" style="position: absolute; height: auto; width: 500px; display: none; top: 381.5px; left: 388.5px;z-index:9999">
			<div class="text-center font-white">
				<h4>테스트 수행 중 입니다.<br>테스트 종료 후 테스트 결과화면으로 이동합니다.</h4>
				<br>
				<img src="${pageContext.servletContext.contextPath}/resources/assets/images/spinner/loader-light.gif" />
			</div>
		</div>
		<div id="loader-overlay"
			class="ui-front loader ui-widget-overlay bg-black opacity-60"
			style="display: none;"></div>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/dataTables.select.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-bootstrap.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-global-settings.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#datatable-slts').dataTable({
					columnDefs : [ {
						orderable : false,
						className : 'select-checkbox',
						targets : 0
					} ],
					select : {
						style : 'multi',
						selector : 'td:first-child'
					},
					order : [ [ 1, 'asc' ] ]
				});
			});
		</script>

		<script type="text/javascript">
			$('#doRDTest').on('click', function() {
				$.ajax({
					url : 'userinsight.co.kr',
					beforeSend : function() {
						$('#loader-overlay').show();
						$('#loader-dialog').show();
					},
					success : function(data) {

					}
				});

			});
		</script>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
