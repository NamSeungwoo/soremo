<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/select.dataTables.min.css">
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<style type="text/css">
.progress-overlay {
	background:
		url(${pageContext.servletContext.contextPath}/resources/assets/images/animated-overlay.gif);
	!
	important
}

#datatable-slts td {
	text-align: center
}
</style>

</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>

			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="column-sort ui-sortable">
										<!-- guideline: 해당 화면에서 결과값을 사용자가 변경할 수 있는 input 요소만 form 전송 시 업데이트 될 수 있도록 개발 -->
										<form action="#" method="post">
											<table class="table table-striped table-bordered"
												id="datatable-slts">
												<thead>
													<tr>
														<th></th>
														<th>#</th>
														<th>테스트 단계</th>
														<th>품질 속성</th>
														<th>RD ID</th>
														<th>입력 값</th>
														<th>실제 결과 값</th>
														<th>비고</th>
													</tr>
												</thead>
												<tbody>
													<tr id="100">
														<td></td>
														<td>1</td>
														<td>시스템 테스트</td>
														<td>기능성</td>
														<td>F-010, M-001</td>
														<td>a=10, b=30, c=1</td>
														<td>8 sec</td>
														<td class="result"><span class="font-green">pass</span></td>
													</tr>
													<tr id="200">
														<td></td>
														<td>1</td>
														<td>시스템 테스트</td>
														<td>기능성</td>
														<td>F-010, M-001</td>
														<td>a=10, b=30, c=1</td>
														<td>3 repeated</td>
														<td class="result"><span class="font-red">failed</span></td>
													</tr>
													<tr id="300">
														<td></td>
														<td>1</td>
														<td>시스템 테스트</td>
														<td>기능성</td>
														<td>F-010, M-001</td>
														<td>a=10, b=30, c=1</td>
														<td><div class="form-group">
																<div class="input-group">
																	<input type="text" name="resultData[2]" data-id="300"
																		class="form-control" placeholder="결과 값 입력"> <span
																		class="input-group-btn">
																		<button id="set-none-btn" data-id="300" data-toggle="tooltip" data-placement="top" title="none"
																			class="btn btn-default" type="button">N</button>
																		<button id="set-pass-btn" data-id="300" data-toggle="tooltip" data-placement="top" title="pass"
																			class="btn btn-primary" type="button">P</button>
																		<button id="set-fail-btn" data-id="300" data-toggle="tooltip" data-placement="top" title="failed"
																			class="btn btn-danger" type="button">F</button>
																	</span>
																</div>
															</div></td>
														<td class="result"><span class="font-gray">none</span></td>
													</tr>
												</tbody>
											</table>
											<div class="button-pane mrg20T row">
												<button type="submit" class="btn btn-info">저장</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="loader-dialog"
			style="position: absolute; height: auto; width: 500px; display: none; top: 381.5px; left: 388.5px; z-index: 9999">
			<div class="text-center font-white">
				<h4>
					테스트 수행 중 입니다.<br>테스트 종료 후 테스트 결과화면으로 이동합니다.
				</h4>
				<br> <img
					src="${pageContext.servletContext.contextPath}/resources/assets/images/spinner/loader-light.gif" />
			</div>
		</div>
		<div id="loader-overlay"
			class="ui-front loader ui-widget-overlay bg-black opacity-60"
			style="display: none;"></div>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/dataTables.select.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-bootstrap.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-global-settings.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#datatable-slts').dataTable({
					columnDefs : [ {
						orderable : false,
						className : 'select-checkbox',
						targets : 0
					} ],
					select : {
						style : 'multi',
						selector : 'td:first-child'
					},
					order : [ [ 1, 'asc' ] ]
				});
			});
		</script>

		<script type="text/javascript">
			$('#doRDTest').on('click', function() {
				$.ajax({
					url : 'userinsight.co.kr',
					beforeSend : function() {
						$('#loader-overlay').show();
						$('#loader-dialog').show();
					},
					success : function(data) {

					}
				});

			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tooltip/tooltip.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tooltip/tooltip-demo.js"></script>
		<script type="text/javascript">
			$('#set-none-btn').on(
					'click',
					function() {
						var id = $(this).attr('data-id');
						$('#' + id).find('.result').html(
								'<span class="font-gray">none</span>');
					});
			$('#set-pass-btn').on(
					'click',
					function() {
						var id = $(this).attr('data-id');
						$('#' + id).find('.result').html(
								'<span class="font-green">pass</span>');
					});

			$('#set-fail-btn').on(
					'click',
					function() {
						var id = $(this).attr('data-id');
						$('#' + id).find('.result').html(
								'<span class="font-red">failed</span>');
					});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
