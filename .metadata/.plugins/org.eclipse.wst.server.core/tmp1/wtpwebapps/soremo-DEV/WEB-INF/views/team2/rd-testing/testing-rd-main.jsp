<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../../commons/meta.jsp"%>
<%@ include file="../../commons/core-css.jsp"%>
<%@ include file="../../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
<style type="text/css">
.ui-sortable-handle .content-box-header {
	cursor: move
}

.ui-dialog-titlebar-show:before {
	content: "\f106" !important
}

.ui-dialog-titlebar-hide:before {
	content: "\f107" !important
}
.ui-dialog, .ui-dialog-titlebar{
	background-color:#f5f5f5 !important;
}
</style>

</head>

<body>
	<div id="sb-site">
		<%@ include file="../../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../../commons/ui/header-ui.jsp"%>
			<%@ include file="../../commons/ui/sidemenu-ui.jsp"%>
			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<div class="panel">
							<div class="panel-body">
								<div class="example-box-wrapper">
									<div class="column-sort ui-sortable">
										<div class="content-box ui-sortable-handle">
											<h3 class="content-box-header bg-default">내적 정보</h3>
											<div class="content-box-wrapper" id="contentList">contents</div>
										</div>

										<div class="content-box ui-sortable-handle">
											<h3 class="content-box-header bg-default">RLIM</h3>
											<div class="content-box-wrapper" id="contentList">
												<table class="table table-hover">
													<thead>
														<tr>
															<th>#</th>
															<th>Row 1</th>
															<th>Row 2</th>
															<th>Row 3</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>#</td>
															<td>Row 1</td>
															<td>Row 2</td>
															<td>Row 3</td>
														</tr>
														<tr>
															<td>#</td>
															<td>Row 1</td>
															<td>Row 2</td>
															<td>Row 3</td>
														</tr>
														<tr>
															<td>#</td>
															<td>Row 1</td>
															<td>Row 2</td>
															<td>Row 3</td>
														</tr>
														<tr>
															<td>#</td>
															<td>Row 1</td>
															<td>Row 2</td>
															<td>Row 3</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="content-box ui-sortable-handle">
											<h3 class="content-box-header bg-default">평가 지침</h3>
											<div class="content-box-wrapper">
												<table class="table table-hover">
													<thead>
														<tr>
															<th>번호</th>
															<th>평가 지침</th>
															<th>평가 여부</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>연구 목적과 필요성, 연구 동향과 사업 내용의 부합</td>
															<td>예</td>
															<td><input type="checkbox" class="input-switch-alt"
																checked>
														</tr>
														<tr>
															<td>2</td>
															<td>연구 수행 조직과 업무 분담, 책임에 대해 적절한 기술</td>
															<td class="font-red">아니오</td>
															<td><input type="checkbox" class="input-switch-alt">
														</tr>
													</tbody>
												</table>
											</div>
										</div>


										<div class="button-pane mrg20T">
											<button class="btn btn-info">저장</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="hide" id="rd-explorer-dialog" title="RD 탐색기">
			<div id="rd-tree">
				<ul>
					<li id="100">Planning
						<ul>
							<li id="101">1</li>
							<li id="102">2</li>
						</ul>
					</li>
					<li id="200">Test Design and Implementation
						<ul>
							<li id="201">Identify Feature List</li>
							<li id="202">Derive Test Conditions</li>
							<li id="203">Derive Test Conditions</li>
							<li id="204">Derive Test Conditions</li>
							<li id="205">Derive Test Conditions</li>
						</ul>
					</li>
					<li id="300">Test Enviroment Set-Up and Maintenance</li>
					<li id="400">Test Execution
						<ul>
							<li>Execute Test Procedures</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>



		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript">
			/* Sortable elements */

			$(function() {
				"use strict";
				$(".sortable-elements").sortable();
			});

			$(function() {
				"use strict";
				$(".column-sort").sortable({
					connectWith : ".column-sort"
				});
			});
		</script>
		<script type="text/javascript">
			$('#rd-tree').jstree();
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/resizable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/draggable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/selectable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog.js"></script>
		<script type="text/javascript">
			var idx = $(this).attr('data-id');
			console.log('선택된 테스트 결과의 id:' + idx);
			$(function() {
				$("#rd-explorer-dialog").dialog(
						{
							modal : false,
							minWidth : 160,
							minHeight : 340,
							dialogClass : "",
							show : "fadeIn",
							closeOnEscape : false,
							position : {
								my : 'left bottom',
								at : 'left center',
								of : document
							},
							close : function(event, ui) {
								$('.ui-dialog-content').toggleClass('hide');
								$('.ui-dialog-titlebar-close').toggleClass(
										'ui-dialog-titlebar-hide');
								$("#rd-explorer-dialog").dialog("open");
							}
						});

				$('.ui-dialog-content').removeClass('hide');
			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>



</body>

</html>
