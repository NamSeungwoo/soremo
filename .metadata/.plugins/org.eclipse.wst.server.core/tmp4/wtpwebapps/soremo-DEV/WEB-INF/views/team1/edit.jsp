<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="../commons/meta.jsp"%>
<%@ include file="../commons/core-css.jsp"%>
<%@ include file="../commons/core-js.jsp"%>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tree/jstree.min.js"></script>
</head>

<body>
	<div id="sb-site">
		<%@ include file="../commons/ui/dashboard-compact.jsp"%>
		<%@ include file="../commons/ui/loader.jsp"%>

		<div id="page-wrapper">
			<%@ include file="../commons/ui/header-ui.jsp"%>
			<%@ include file="../commons/ui/sidemenu-ui.jsp"%>

			<script>
				$('#rd_tree').jstree();
			</script>

			<script type="text/javascript"
				src="${pageContext.servletContext.contextPath}/resources/assets/widgets/ckeditor/ckeditor.js"></script>

			<script>
				// This code is generally not necessary, but it is here to demonstrate
				// how to customize specific editor instances on the fly. This fits well
				// this demo because we have editable elements (like headers) that
				// require less features.

				// The "instanceCreated" event is fired for every editor instance created.
				CKEDITOR
						.on(
								'instanceCreated',
								function(event) {
									var editor = event.editor, element = editor.element;

									// Customize editors for headers and tag list.
									// These editors don't need features like smileys, templates, iframes etc.
									if (element.is('h1', 'h2', 'h3')
											|| element.getAttribute('id') == 'taglist') {
										// Customize the editor configurations on "configLoaded" event,
										// which is fired after the configuration file loading and
										// execution. This makes it possible to change the
										// configurations before the editor initialization takes place.
										editor
												.on(
														'configLoaded',
														function() {

															// Remove unnecessary plugins to make the editor simpler.
															editor.config.removePlugins = 'colorbutton,find,flash,font,'
																	+ 'forms,iframe,image,newpage,removeformat,'
																	+ 'smiley,specialchar,stylescombo,templates';

															// Rearrange the layout of the toolbar.
															editor.config.toolbarGroups = [
																	{
																		name : 'editing',
																		groups : [
																				'basicstyles',
																				'links' ]
																	},
																	{
																		name : 'undo'
																	},
																	{
																		name : 'clipboard',
																		groups : [
																				'selection',
																				'clipboard' ]
																	},
																	{
																		name : 'about'
																	} ];
														});
									}
								});
			</script>

			<div id="page-content-wrapper">
				<div id="page-content">
					<div class="container pad10A">
						<textarea class="ckeditor" cols="80" id="editor1" name="editor1"
							rows="50"></textarea>
					</div>
				</div>
			</div>
		</div>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/resizable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/draggable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/sortable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/interactions-ui/selectable.js"></script>

		<script type="text/javascript">
			/* Sortable elements */

			$(function() {
				"use strict";
				$(".sortable-elements").sortable();
			});

			$(function() {
				"use strict";
				$(".column-sort").sortable({
					connectWith : ".column-sort"
				});
			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tooltip/tooltip.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/popover/popover.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/xeditable/xeditable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/xeditable/xeditable-demo.js"></script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/dialog/dialog-demo.js"></script>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title">Select Target RD</h4>
					</div>
					<div class="modal-body">
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered"
							id="datatable-target-rd">
							<thead>
								<tr>
									<th>RD Name</th>
									<th>Contents</th>
									<th>Process</th>
									<th>RD Type</th>
								</tr>
							</thead>
							<tbody>
								<tr class="odd gradeX">
									<td class="target-rd-clickable">RD 설계</td>
									<td>System</td>
									<td>Evaluation</td>
									<td>Text</td>
								</tr>
								<tr class="even gradeC">
									<td class="target-rd-clickable">RD 분석</td>
									<td>Simulate</td>
									<td>Analysis</td>
									<td>Text</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-bootstrap.js"></script>
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/datatable/datatable-fixedcolumns.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				var table = $('#datatable-target-rd');

				table.dataTable({
					paging : false,
					info : false,
					"search" : {
						"search" : "Search Target RD:"
					}
				});
			});

			$('.target-rd-clickable').on('click', function() {
				$('#target-rd-name').html($(this).html());
				$('#myModal').modal('hide');
			});
		</script>

		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets/widgets/tabs-ui/tabs.js"></script>
		<script type="text/javascript">
			/* jQuery UI Tabs */

			$(function() {
				"use strict";
				$(".tabs").tabs();
			});

			$(function() {
				"use strict";
				$(".tabs-hover").tabs({
					event : "mouseover"
				});
			});
		</script>

		<!-- JS Demo -->
		<script type="text/javascript"
			src="${pageContext.servletContext.contextPath}/resources/assets-minified/admin-all-demo.js"></script>
	</div>
</body>

</html>
